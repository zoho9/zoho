<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
<title>note mail</title>

<style type="text/css">

	    body{width: 100%; background-color: #f0f0f0; margin:0; padding:0; -webkit-font-smoothing: antialiased;mso-margin-top-alt:0px; mso-margin-bottom-alt:0px; mso-padding-alt: 0px 0px 0px 0px;}
        
        p,h1,h2,h3,h4{margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;}
        
        span.preheader{display: none; font-size: 1px;}
        
        html{width: 100%;}
        
        table{font-size: 12px;border: 0;}
		
		.menu-space{padding-right:25px;}
		
		a,a:hover { text-decoration:none; color:#FFF;}


@media only screen and (max-width:640px)

{
	body{width:auto!important;}
	table[class=main] {width:440px !important;}
	table[class=two-left] {width:420px !important; margin:0px auto;}
	table[class=full] {width:100% !important; margin:0px auto;}
	table[class=alaine] { text-align:center;}
	table[class=menu-space] {padding-right:0px;}
	table[class=banner] {width:438px !important;}
	table[class=menu] {width:438px !important; margin:0px auto; border-bottom:#e1e0e2 solid 1px;}
	table[class=date] {width:438px !important; margin:0px auto; text-align:center;}
	table[class=two-left-inner] {width:400px !important; margin:0px auto;}
	table[class=menu-icon] { display:block;}
	table[class=two-left-menu] {text-align:center;}

	}

@media only screen and (max-width:479px)
{
	body{width:auto!important;}
	table[class=main]  {width:310px !important;}
	table[class=two-left] {width:300px !important; margin:0px auto;}
	table[class=full] {width:100% !important; margin:0px auto;}
	table[class=alaine] { text-align:center;}
	table[class=menu-space] {padding-right:0px;}
	table[class=banner] {width:308px !important;}
	table[class=menu] {width:308px !important; margin:0px auto; border-bottom:#e1e0e2 solid 1px;}
	table[class=date] {width:308px !important; margin:0px auto; text-align:center;}
	table[class=two-left-inner] {width:280px !important; margin:0px auto;}
	table[class=menu-icon] { display:none;}
	table[class=two-left-menu] {width:310px !important; margin:0px auto;}

	
}



</style>

</head>

<body yahoo="fix" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!--Main Table Start-->

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#f0f0f0" style="background:#f0f0f0;">
  <tr>
    <td align="center" valign="top">
    



<!--Text Part Start-->

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" valign="top"><table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
      <tr>
        <td align="center" valign="middle" bgcolor="#FFFFFF" style="-moz-border-radius: 0px 0px 8px 8px; border-radius: 0px 0px 8px 8px;"><table width="490" border="0" cellspacing="0" cellpadding="0" class="two-left-inner">
          <tr>
             <td height="60" align="center" valign="middle" style="line-height:60px; font-size:60px;">&nbsp;</td>
          </tr>
             <tr>
            <td height="30" align="center" valign="middle" style="line-height:30px; font-size:30px;">&nbsp;</td>
          </tr>
          
          <tr>
            <td align="center" valign="top" style="font-family:'Open Sans', Verdana, Arial; font-size:26px; color:#121212; font-weight:normal;">
              Assalaamu Alaikum</td>
          </tr>
          <tr>
            <td height="30" align="center" valign="middle" style="line-height:20px; font-size:20px;">&nbsp;</td>
          </tr>
           <tr>
            <td align="left" valign="top" style="font-family:'Open Sans', Verdana, Arial; font-size:18px; color:#121212; font-weight:normal;"> Dear {{ $data['name'] }},</td>
          </tr>
          <tr>
            <td height="15" align="center" valign="middle" style="line-height:10px; font-size:10px;">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:'Open Sans', Verdana, Arial; font-size:12px; color:#767676; font-weight:normal; line-height:24px;">
              Thank you for sending us your qard hasan loan receipts and correspondence.</td>
          </tr>
          <tr>
            <td height="10" align="center" valign="middle" style="line-height:10px; font-size:10px;">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:'Open Sans', Verdana, Arial; font-size:12px; color:#767676; font-weight:normal; line-height:24px;">We can confirm that your current outstanding loan amount is: {{ $data['loan'] }}</td>
          </tr>
          <tr>
            <td height="10" align="center" valign="middle" style="line-height:10px; font-size:10px;">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:'Open Sans', Verdana, Arial; font-size:12px; color:#767676; font-weight:normal; line-height:24px;">Alhamdulillah we can confirm that a monthly standing order for your qard hasan loan repayment has been set up.</td>
          </tr>
          <tr>
            <td height="10" align="center" valign="middle" style="line-height:10px; font-size:10px;">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:'Open Sans', Verdana, Arial; font-size:12px; color:#767676; font-weight:normal; line-height:24px;">The monthly amount is </td>
          </tr>
          <tr>
            <td height="10" align="center" valign="middle" style="line-height:10px; font-size:10px;">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:'Open Sans', Verdana, Arial; font-size:12px; color:#767676; font-weight:normal; line-height:24px;">The money will be paid to the following bank account;</td>
          </tr>
          <tr>
            <td height="5" align="center" valign="middle" style="line-height:5px; font-size:10px;">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:'Open Sans', Verdana, Arial; font-size:12px; color:#767676; font-weight:normal; line-height:24px;">Name: {{ $data['name'] }} <br>
              Account Number: {{ $data['account_number'] }} <br>
              Sort Code:{{ $data['bank_short_code'] }}  </td>
          </tr>
          <tr>
            <td height="10" align="center" valign="middle" style="line-height:10px; font-size:10px;">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:'Open Sans', Verdana, Arial; font-size:12px; color:#767676; font-weight:normal; line-height:24px;">Payment start date:<br>
              Number of Payments:
          </tr>
          <tr>
            <td height="10" align="center" valign="middle" style="line-height:10px; font-size:10px;">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:'Open Sans', Verdana, Arial; font-size:12px; color:#767676; font-weight:normal; line-height:24px;">Please check that your bank details are correct. If there is any error with the bank details please get back to us immediately.</td>
          </tr>
          <tr>
            <td height="10" align="center" valign="middle" style="line-height:10px; font-size:10px;">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:'Open Sans', Verdana, Arial; font-size:12px; color:#767676; font-weight:normal; line-height:24px;">The monthly payment amount will be reviewed after next Ramadhan and may be increased depending on the amount of funds available for qard hasan loan repayments.</td>
          </tr>
          <tr>
            <td height="10" align="center" valign="middle" style="line-height:10px; font-size:10px;">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:'Open Sans', Verdana, Arial; font-size:12px; color:#767676; font-weight:normal; line-height:24px;">We thank you for your patience and continued support of Ebrahim College.</td>
          </tr>
          <tr>
            <td height="10" align="center" valign="middle" style="line-height:10px; font-size:10px;">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-family:'Open Sans', Verdana, Arial; font-size:12px; color:#767676; font-weight:normal; line-height:24px;">Qard Hasan Team</td>
          </tr>

      
          
          <tr>
             <td height="50" align="center" valign="middle" style="line-height:50px; font-size:50px;">&nbsp;</td>
          </tr>

        </table></td>
      </tr>
    </table></td>
  </tr>
</table>

<!--Text Part End-->


    </td>
  </tr>
</table>

<!--Main Table End-->

</body>
</html>
