<!DOCTYPE html>
<html>
<head>
    <title>Hi</title>
</head>
<body>
    <div class="container">

<div class="table-responsive overflow-initial ">
                    <table class="header-fixed table zi-table table-hover ember-view ">
                        <thead>
                        <tr>
                            <th class="sortable text-left ember-view" style="width:100px;">
                                Date
                            </th>
                            <th class="sortable text-left ember-view" style="width:120px;">
                                Invoice No 
                            </th>
                            <th class="text-center ember-view text-center" style="width:80px;">
                                Amount
                            </th>
                            <th class="text-center ember-view text-center" style="width:120px;">
                                Payment Mode
                            </th>
                            <!--<th class="text-center ember-view text-center" style="width:120px;">
                                Payment Received
                            </th>
                            <th class="text-center ember-view text-center" style="width:120px;">
                                Send Message
                            </th>-->
                         </tr>
                      </thead>
                      <tbody>
                        @foreach($invoice__ AS $list)
                        <tr class="ember-view">
                            <td>{{date("d M, Y",strtotime($list->payment_dt))}}</td>
                            <td><a href="{{url('borrowers/invoice/view')}}/{{$list->id}}">{{$list->invoice_no}}</a></td>
                            <td class="text-center">{{number_format($list->amount,2)}}</td>
                            <td class="text-center">{{$list->payment_mode}}</td>
                            <!--<td class="text-center">{{ucwords($list->is_receive)}}</td>
                            <td class="text-center">{{ucwords($list->is_mail)}}</td>-->
                        </tr>
                        @endforeach
                      </tbody>
                   </table>
                </div>
                </div>
</body>
</html>